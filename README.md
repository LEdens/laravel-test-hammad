## How to run

- Run composer install.
- Run php artisan key:generate.
- Run npm install
- Run npm dev (optional)
- Run php artisan migrate
- Run php artisan db:seed --class=UserSeeder

## Features

- This project possess both front-end web-app as well as api based app
