<?php

namespace App\Http\Controllers;

use App\Mail\NewPinMail;
use App\Models\User;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserController extends Controller
{
    use UploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $token = Str::random(80);

        // Auth::user()->forceFill([
        //     'api_token' => $token,
        // ])->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();

        $request->validate([
            'user_name' => ['required','string','min:4','max:20','regex:/^(([a-zA-Z0-9_.]*)([a-zA-Z0-9_.]*))+$/','unique:users,user_name,' . $user->id],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $user->id],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
            'avatar' => ['nullable', 'image', 'mimes:jpeg,png,jpg', 'dimensions:width=256,height=256'],
        ]);

        if ($request->has('avatar')) {
            // Get image file
            $image = $request->file('avatar');
            // Make a image name based on user name and current timestamp
            $name =  'profile_' . time();
            // Define folder path
            $folder = '/uploads/images/users/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();

            // Fit the created image to required ratio and save that image
            $this->uploadOne($image, $folder, 'public', $name);

            // Delete exsisting image
            $this->deleteOne('public', $user->cover_image);
            // Set user profile image path in database to filePath

            $user->avatar = $filePath;

        }

        $user->user_name= $request->user_name;
        $user->email= $request->email;
        
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        $user->save();
        
        if (\Request::is('api*')) {
            return response()->json(['message' => 'Updated!'], 200);
        }

        return back()->with(['status' => 'Updated!']);
    }

    /**
     * Show pin form
     */
    public function pin()
    {
        return view('user.pin-form');
    }

    /**
     * Verify Pin
     */
    public function verifyingPin(Request $request)
    {
        $request->validate([
            'pin' => 'required|max:6|min:6',
        ]);

        if (Auth::user()->pin != $request->pin) {
            if (\Request::is('api*')) {
                return response()->json(['message' => 'Invalid!'], 422);
            }

            return back()->with(['status_error' => 'Invalid!']);
        }

        Auth::user()->update([
            'pin_verified_at' => Carbon::now(),
        ]);

        if (\Request::is('api*')) {
            return response()->json(['message' => 'Verified!'], 200);
        }

        return back()->with(['status' => 'Verified!']);
    }

    /**
     * Generate New pin
     */
    public function newPin()
    {
        $user = Auth::user();
        $pin = Str::random(6);

        $user->forceFill([
            'pin' => $pin,
        ])->save();

        Mail::to($user->email)->send(new NewPinMail($pin));

        if (\Request::is('api*')) {
            return response()->json(['message' => 'New Pin sent!'], 200);
        }

        return back()->with(['status' => 'New Pin sent!']);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
