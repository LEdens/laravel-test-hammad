<?php

namespace App\Http\Controllers;

use App\Mail\InvitationEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class InvitationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['adminChecker']);
    }

    /**
     * Display a form to send email invites to users
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('invitation');
    }

    /**
     * Send email to the specified email addresses
     * 
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {
        $request->validate([
            'emails' => 'required',
        ]);
    
        foreach ($request->emails as $email) {
            
            $data = [
                'email'=> $email,
                'message' => $request->message,
            ];

            Mail::to($email)->send(new InvitationEmail($data));
        }

        if (\Request::is('api*')) {
            return response()->json(['message' => 'Invitation(s) has been sent!'], 200);
        }

        return back()->with(['status'=> 'Invitation(s) has been sent!']);
    }
}
