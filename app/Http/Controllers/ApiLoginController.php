<?php

namespace App\Http\Controllers;

use App\Mail\NewPinMail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ApiLoginController extends Controller
{
    /**
     * Authenticated user and create API token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function login(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');
        $user        = User::where('email', $credentials['email'])->first();

        if ($validatedData->fails()) {
            return response()->json(['data' => $validatedData->messages()->getMessages()], 422);
        } else {
            if (Auth::attempt($credentials)) {

                $token = Str::random(80);

                $user->forceFill([
                    'api_token' => $token,
                ])->save();

                return response()->json(['data' => 'success', 'token' => $token, 'id' => $user->id, 'role' => $user->user_role]);
            }
            return response()->json(['message' => 'The given data was invalid', 'errors' => ['email' => ['These credentials do not match our records.']]], 422);
        }
    }

    /**
     * Destroy user's API token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function logout(Request $request)
    {
        $user = $request->user();

        $user->forceFill([
            'api_token' => null
        ])->save();

        return response()->json(['message' => 'Logged out!'], 200);
    }

    /**
     * Register the user
     */
    public function register(Request $request)
    {
        $request->validate([
            'user_name' => ['required', 'string', 'min:4', 'max:20', 'unique:users', 'regex:/^(([a-zA-Z0-9_.]*)([a-zA-Z0-9_.]*))+$/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        
        $token = Str::random(80);
        $pin = Str::random(6);

        $user = User::create([
            'user_name' => $request->user_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'avatar' => null,
            'user_role' => 'user',
            'registered_at' => Carbon::now(),
        ]);

        $user->forceFill([
            'api_token' => $token,
            'pin' => $pin,
        ])->save();

        Mail::to($user->email)->send(new NewPinMail($pin));

        return response()->json(['data' => 'Your accout has been created, a pin has been sent to your email please check your email and verify your pin.', 'token' => $token, 'id' => $user->id, 'role' => $user->user_role]);
    }
}
