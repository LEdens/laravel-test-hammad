<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PinVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if (auth()->check() && auth()->user()->pin_verified_at != null) {
            return $response;
        } else {
            if (\Request::is('api*')) {
                return response()->json(['message' => 'Opps! You have yet to verify your pin.'], 401);
            } else {
                return redirect('/verify-pin')->withStatusError('Opps! You have yet to verify your pin.');
            }
        }
    }
}
