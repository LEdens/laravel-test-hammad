<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if (auth()->check() && auth()->user()->user_role == 'admin') {
            return $response;
        } else {
            return response()->json(['message' => 'Opps! You are not allowed.'], 401);
        }
    }
}
