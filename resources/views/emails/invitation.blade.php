<div>
    @php
        if($data['message']) {
            $message = $data['message'];
        } else {
            $message = "We invite you to get registered on our website. Please click the following link";
        }
    @endphp

    {{ $message }} <a href="{{ url('/register?email=' . $data['email']) }}">click here</a>
    
</div>