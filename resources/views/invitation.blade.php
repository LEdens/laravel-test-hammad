@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Invidation form') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form method="POST" action="{{ route('invite.send') }}">
                        @csrf
                        <div class="form-group">
                            <label for="emails">Email addresses *</label>
                            <select class="form-control" name="emails[]" id="emails" placeholder="Enter email(s)" multiple required> </select>
                        </div>
                        <div class="form-group">
                            <label for="message">Custom Message</label>
                            <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Don't worry about the link it will be added automatically"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
