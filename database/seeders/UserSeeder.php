<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'user_name' => 'Admin User',
            'email' => 'admin@example.com',
            'avatar' => null,
            'user_role' => 'admin',
            'password' => Hash::make('password'),
            'registered_at' => Carbon::now(),
        ]);
    }
}
