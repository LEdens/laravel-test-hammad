<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [App\Http\Controllers\ApiLoginController::class, 'login']);
Route::post('/register', [App\Http\Controllers\ApiLoginController::class, 'register']);

Route::middleware(['auth:api', 'pinVerified'])->prefix('admin')->group(function () {
    Route::get('send-invites', [App\Http\Controllers\InvitationController::class, 'index'])->name('invite');
    Route::post('sending', [App\Http\Controllers\InvitationController::class, 'send'])->name('invite.send');
});

Route::middleware(['auth:api', 'pinVerified'])->group(function () {
    Route::get('edit-profile', [App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');
    Route::post('updating-profile', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');
});

Route::middleware('auth:api')->group(function () {
    Route::post('/logout', [App\Http\Controllers\ApiLoginController::class, 'logout']);

    Route::post('new-pin', [App\Http\Controllers\UserController::class, 'newPin'])->name('user.new.pin');
    Route::post('pin-verification', [App\Http\Controllers\UserController::class, 'verifyingPin'])->name('user.verifying.pin');
});
