<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('admin')->middleware(['auth', 'pinVerified'])->group(function () {
    Route::get('send-invites', [App\Http\Controllers\InvitationController::class, 'index'])->name('invite');
    Route::post('sending', [App\Http\Controllers\InvitationController::class, 'send'])->name('invite.send');
});

Route::middleware(['auth', 'pinVerified'])->group(function () {
    Route::get('edit-profile', [App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');
    Route::put('updating-profile', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');
});

Route::middleware(['auth'])->group(function () {
    Route::get('verify-pin', [App\Http\Controllers\UserController::class, 'pin'])->name('user.pin');
    Route::post('pin-verification', [App\Http\Controllers\UserController::class, 'verifyingPin'])->name('user.verifying.pin');
});